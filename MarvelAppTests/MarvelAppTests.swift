//
//  MarvelAppTests.swift
//  MarvelAppTests
//
//  Created by Tomek Wojtyniak on 04/06/2021.
//

import XCTest
@testable import MarvelApp

class MarvelAppTests: XCTestCase {
    

    var sut: ViewModelProtocol!
    var mockAPIClient: MockAPIClient!
    
    
    override func setUpWithError() throws {
        super.setUp()
        sut = MainPageViewModel()



    }

    override func tearDownWithError() throws {
        sut = nil
        mockAPIClient = nil
        super.tearDown()
    }
    
    func test_fetchAPI(){
        mockAPIClient = MockAPIClient(mockResults: [Result(id: 1, title: "title", description: nil, images: nil, creators: nil)])
        sut.apiClient = mockAPIClient

        sut.getAPIClient()
        XCTAssert(mockAPIClient!.isAPIDataFetched)
    }
    
    func test_fetchAPI_get_apiResult(){
        mockAPIClient = MockAPIClient(mockResults: [Result(id: 11, title: "title", description: "desc", images: nil, creators: nil)])
        sut.apiClient = mockAPIClient
    
        sut.getAPIClient()
        
        XCTAssertFalse(mockAPIClient.mockResults.isEmpty)
    }
    
    func test_fetchAPI_get_only_nil(){
        mockAPIClient = MockAPIClient()
        sut.apiClient = mockAPIClient
        sut.getAPIClient()
        
        XCTAssert(mockAPIClient.mockResults.isEmpty)
        
        
    }
    
}

class MockAPIClient: APIClientProtocol {
    var mockResults: [Result] = []
    
    init(mockResults: [Result]? = nil){
        self.mockResults = mockResults ?? []
    }
    
    var isAPIDataFetched = false
    
    
    func fetchData(completion: @escaping (([Result]) -> Void)) {

        isAPIDataFetched = true
    }
    
    func fetchSearchData(title: String, completion: @escaping (([Result]) -> Void)) {
        isAPIDataFetched = true
        
    }

}
