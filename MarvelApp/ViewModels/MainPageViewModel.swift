//
//  MainPageViewModel.swift
//  MarvelApp
//
//  Created by Tomasz Wojtyniak on 13/07/2021.
//

import Foundation
import UIKit
import Kingfisher

public class MainPageViewModel: ViewModelProtocol  {

    public var apiClient: APIClientProtocol?
    
    public var apiResults: [Result] = []
    

    public var  viewModelDelegate: ViewModelDelegate?
    
        
    public func title(indexPathRow: Int) -> String {
        return apiResults[indexPathRow].title ?? " "
    }
    
    public func writters(indexPathRow: Int) -> String {
        

        guard let countCreators: Int = apiResults[indexPathRow].creators?.items?.count else { return " " }

        var creators =  ""
        
        if apiResults[indexPathRow].creators?.available != 0{

            for i in 0..<countCreators {
                if apiResults[indexPathRow].creators?.items?[i].role == "writer"{
                    creators += apiResults[indexPathRow].creators?.items?[i].name ?? " "
                    creators +=  ", "
                }
            }
            
        }
        return creators
    }

    public func cover(indexPathRow: Int) -> String {
        var imageURL = Constant.blankImageURL
        let imageArray = apiResults[indexPathRow].images
        if imageArray?.count != 0{
            
            imageURL = (imageArray?[0].path ?? " ") + "." + (imageArray?[0].extension ?? " ")
        }
        
        return imageURL
    }
    
    public func description(indexPathRow: Int) -> String {

        return apiResults[indexPathRow].description ?? " "
    }
    
    public func getAPIClient(){
        apiClient?.fetchData() { comics in
            self.apiResults = comics
            self.viewModelDelegate?.didDataLoaded()
        }
    }
    
    public func getSearchAPIClient(title: String){
        apiClient?.fetchSearchData(title: title) { comics in
            self.apiResults = comics
            self.viewModelDelegate?.didSearchDataLoaded(title: title, count: self.apiResults.count)
        }
    }
    
}
    


public protocol ViewModelProtocol {
    func cover(indexPathRow: Int) -> String
    
    func writters(indexPathRow: Int) -> String
    
    func title(indexPathRow: Int) -> String
    
    func description(indexPathRow: Int) -> String
    
    var apiResults: [Result] { get }
    
    var viewModelDelegate: ViewModelDelegate? { get set }
    
    func getAPIClient()
    
    func getSearchAPIClient(title: String)
    
    var apiClient: APIClientProtocol? { get set }
}

public protocol ViewModelDelegate {
    func didDataLoaded()
    
    func didSearchDataLoaded(title: String, count: Int)
    
}

