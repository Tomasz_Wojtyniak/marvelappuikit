//
//  ComicsViewController.swift
//  MarvelApp
//
//  Created by Tomek Wojtyniak on 05/06/2021.
//

import UIKit
import Kingfisher


class ComicsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var comicsTableView: UITableView!
    
    var comicsViewModel: ViewModelProtocol?
    
    
    override func viewDidLoad() {

        
        super.viewDidLoad()
        comicsViewModel?.getAPIClient()
        
        comicsViewModel?.viewModelDelegate = self
        comicsTableView.delegate = self
        comicsTableView.dataSource = self
        comicsTableView.register(ComicsTableViewCell.self, forCellReuseIdentifier: ComicsTableViewCell.imageIdentifier)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComicsCell") as! ComicsTableViewCell

        guard let comicsViewModel = comicsViewModel.self else { return UITableViewCell()}
        //Comics table design
        comicsTableView.separatorStyle = .none
        comicsTableView.showsVerticalScrollIndicator = false
        
        //Comics cell design
        cell.comicsView.layer.shadowOffset = Constant.cellShadowOffset
        cell.comicsView.layer.shadowColor = Constant.cellShadowColor
        cell.comicsView.layer.shadowRadius = Constant.cellShadowRadius
        cell.comicsView.layer.shadowOpacity = Constant.cellShadowOpacity
        cell.comicsView.layer.masksToBounds = false
        cell.comicsView.clipsToBounds = false
        cell.comicsDescription.sizeToFit()
    
        cell.comicsImage.layer.cornerRadius = Constant.comicsCoverCornerRadius
        
        cell.comicsTitle.text = comicsViewModel.title(indexPathRow: indexPath.row)
        cell.comicsDescription.text = comicsViewModel.description(indexPathRow: indexPath.row)
        cell.comicsWriter.text = comicsViewModel.writters(indexPathRow: indexPath.row)
        let imageURL = comicsViewModel.cover(indexPathRow: indexPath.row)
        KF.url(URL(string: imageURL)).set(to: cell.comicsImage)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constant.comicsTableViewHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        comicsViewModel?.apiResults.count ?? 0
    }
}

extension ComicsViewController: ViewModelDelegate {

    
    func didSearchDataLoaded(title: String, count: Int) {
        
    }
    

    func didDataLoaded(){
        comicsTableView.reloadData()
    }
    
}
