//
//  SearchViewController.swift
//  MarvelApp
//
//  Created by Tomek Wojtyniak on 07/06/2021.
//

import UIKit
import Kingfisher


class SearchViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{

    
        
    @IBOutlet var comicsTableView: UITableView!
    
    let searchBar = UISearchBar()
    
    let emptySearchText = UILabel()
    
    var comicsViewModel: ViewModelProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        comicsViewModel?.viewModelDelegate = self
        
        //adding searchBar
        searchBar.delegate = self
        view.addSubview(searchBar)
        
        //adding label
        emptySearchText.text = "Start typing to find particular comics"
        view.addSubview(emptySearchText)
        
        //adding image
        view.addSubview(Constant.searchViewBookIcon)

        //show or hide image or table
        emptySearchText.isHidden =  false
        comicsTableView.isHidden = true
        Constant.searchViewBookIcon.isHidden = false

        //comics table setup and design
        comicsTableView.delegate = self
        comicsTableView.dataSource = self
        comicsTableView.separatorStyle = .none
        comicsTableView.showsVerticalScrollIndicator = false
        comicsTableView.register(ComicsTableViewCell.self, forCellReuseIdentifier: ComicsTableViewCell.imageIdentifier)
        
        //keyboard dismiss
        comicsTableView.keyboardDismissMode = .onDrag
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        comicsViewModel?.apiResults.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constant.comicsTableViewHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComicsCell") as! ComicsTableViewCell

        guard let comicsViewModel = comicsViewModel.self else { return UITableViewCell()}
        
        cell.comicsView.layer.shadowOffset = Constant.cellShadowOffset
        cell.comicsView.layer.shadowColor = Constant.cellShadowColor
        cell.comicsView.layer.shadowRadius = Constant.cellShadowRadius
        cell.comicsView.layer.shadowOpacity = Constant.cellShadowOpacity
        cell.comicsView.layer.masksToBounds = false
        cell.comicsView.clipsToBounds = false
        

        cell.comicsImage.layer.cornerRadius = Constant.comicsCoverCornerRadius
        

        cell.comicsTitle.text = comicsViewModel.title(indexPathRow: indexPath.row)
        cell.comicsDescription.text = comicsViewModel.description(indexPathRow: indexPath.row)
        cell.comicsWriter.text = comicsViewModel.writters(indexPathRow: indexPath.row)
        KF.url(URL(string: comicsViewModel.cover(indexPathRow: indexPath.row))).set(to: cell.comicsImage)


        return cell
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //search design and layout
        searchBar.layer.borderWidth = Constant.searchBarBorderWidth
        searchBar.layer.borderColor = Constant.searchBarBorderColor
        searchBar.frame = CGRect(x: Constant.searchBarFrameX,
                                 y: view.safeAreaInsets.top + Constant.searchBarFrameY,
                                 width: view.frame.size.width - Constant.searchBarFrameWidth,
                                 height: Constant.searchBarFrameHeight)
        searchBar.placeholder = Constant.searchBarPlaceholder
        searchBar.tintColor = Constant.searchBarTintColor
        
        //label design and layout
        emptySearchText.textAlignment = .center
        emptySearchText.numberOfLines = Constant.emptySearchTextNumberOfLines
        emptySearchText.frame = CGRect(x: Constant.emptySearchTextFrameX,
                                       y: Constant.emptySearchTextFrameY,
                                       width: self.view.bounds.size.width,
                                       height: self.view.bounds.size.height)
        emptySearchText.font = Constant.emptySearchTextFont
        
        //book image layout
        Constant.searchViewBookIcon.frame = CGRect(x: self.view.frame.width/2 - Constant.searchViewBookIconWidth/2,
                                                   y: self.view.frame.height/2 - Constant.searchViewBookIconHeight/2,
                                                   width: Constant.searchViewBookIconWidth,
                                                   height: Constant.searchViewBookIconHeight)
        
    }
    

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let text = searchBar.text {
            DispatchQueue.main.async {
                self.emptySearchText.isHidden =  true
                self.comicsTableView.isHidden = false
                Constant.searchViewBookIcon.isHidden = true
            }
            comicsViewModel?.getSearchAPIClient(title: text)
        }
    }
}


extension SearchViewController: ViewModelDelegate {
    

    
    func didSearchDataLoaded(title: String, count: Int) {
        if count == 0 {
             DispatchQueue.main.async {
                self.emptySearchText.text = "There is no comic book \"\(title)\" in our library. Check the spelling and try again. "
                self.emptySearchText.isHidden =  false
                self.comicsTableView.isHidden = true
                Constant.searchViewBookIcon.isHidden = false
                
             }
         }
        comicsTableView.reloadData()
    }
    
    
    func didDataLoaded() {
        comicsTableView.reloadData()
        
    }

}

