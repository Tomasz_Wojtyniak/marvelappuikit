//
//  MarvelModel.swift
//  MarvelApp
//
//  Created by Tomasz Wojtyniak on 13/07/2021.
//

import Foundation
import UIKit

struct APIResponse: Codable {
    let code: Int
    let status: String
    let copyright: String
    let attributionText: String
    let attributionHTML:String
    let etag: String
    let data: Data
}

struct Data: Codable {
    let count: Int
    let results: [Result]
}

public struct Result: Codable {
    let id: Int?
    let title: String?
    let description: String?
    let images: [Image]?
    let creators: Creator?
}

struct Creator: Codable {
    let items: [Item]?
    let available: Int
}

struct Item: Codable{
    let name: String
    let role: String
}

struct Image: Codable {
    let path: String
    let `extension`: String
}



