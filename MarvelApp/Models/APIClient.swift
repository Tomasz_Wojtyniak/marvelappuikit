//
//  APIClient.swift
//  MarvelApp
//
//  Created by Tomasz Wojtyniak on 21/07/2021.
//

import Foundation
import UIKit

class APIClient: APIClientProtocol{
    
    var urlString = Constant.urlString
    
    
    public func fetchData( completion: @escaping ([Result]) -> Void){
        
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        config.httpAdditionalHeaders = [
            "Content-Type": Constant.apiContentType,
            "Authorization": Constant.authorization
        ]
        guard let url = URL(string: urlString) else {
            return
        }
        let task = session.dataTask(with: url) { [weak self] data, _, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let JSONResult = try JSONDecoder().decode(APIResponse.self, from: data)
                DispatchQueue.main.async {
                    completion(JSONResult.data.results)
                }
            } catch {
                print(error)
                completion([])
            }
        }
        
        task.resume()
    }
    
    public func fetchSearchData(title: String, completion: @escaping ([Result]) -> Void){
        let urlSearch = Constant.urlSearchString + title

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        config.httpAdditionalHeaders = [
            "Content-Type": Constant.apiContentType,
            "Authorization": Constant.authorization
        ]
        
        guard let url = URL(string: urlSearch) else {
            return
        }
        let task = session.dataTask(with: url) { [weak self] data, _, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                let JSONResult = try JSONDecoder().decode(APIResponse.self, from: data)
                DispatchQueue.main.async {
                    completion(JSONResult.data.results)
                }
            } catch {
                print(error)
            }
        }
        task.resume()
    }
}

public protocol APIClientProtocol {
    func fetchData(completion: @escaping (([Result]) -> Void))
    func fetchSearchData(title: String, completion: @escaping (([Result]) -> Void))
}
