//
//  LayoutVariables.swift
//  MarvelApp
//
//  Created by Tomasz Wojtyniak on 15/07/2021.
//

import Foundation
import UIKit

struct Constant {
    static let comicsCoverCornerRadius: CGFloat = 8.0
    static let comicsTableViewHeight: CGFloat = 200
    static let searchViewBookIcon = UIImageView(image: UIImage(imageLiteralResourceName: "book-open_icon"))
    static let searchBarPlaceholder = "Search for a comic book"
    static let cellShadowOffset = CGSize(width: 0, height: 0)
    static let cellShadowColor = UIColor.black.cgColor
    static let cellShadowRadius: CGFloat = 10
    static let cellShadowOpacity: Float = 0.1
    static let searchBarBorderWidth: CGFloat = 1
    static let searchBarBorderColor = UIColor.white.cgColor
    static let searchBarFrameX: CGFloat = 10
    static let searchBarFrameY: CGFloat = 20
    static let searchBarFrameWidth: CGFloat = 20
    static let searchBarFrameHeight: CGFloat = 40
    static let searchBarTintColor = UIColor.systemRed
    static let emptySearchTextNumberOfLines = 0
    static let emptySearchTextFrameX: CGFloat = 0
    static let emptySearchTextFrameY: CGFloat = 50
    static let emptySearchTextFont = UIFont.systemFont(ofSize: 18)
    static let searchViewBookIconWidth: CGFloat = 80
    static let searchViewBookIconHeight: CGFloat = 62
    static let urlString = "https://gateway.marvel.com/v1/public/comics?ts=1&apikey=080a502746c8a60aeab043387a56eef0&hash=6edc18ab1a954d230c1f03c590d469d2&limit=25&offset=0&orderBy=-onsaleDate"
    static let urlSearchString = "https://gateway.marvel.com/v1/public/comics?ts=1&apikey=080a502746c8a60aeab043387a56eef0&hash=6edc18ab1a954d230c1f03c590d469d2&limit=25&offset=0&orderBy=-onsaleDate&titleStartsWith="
    static let blankImageURL = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Solid_white.svg/2048px-Solid_white.svg.png"
    static let apiContentType = "application/json"
    static let authorization = "914341525b1b3e445c89b40c38442a2b8d0680f7"
}
